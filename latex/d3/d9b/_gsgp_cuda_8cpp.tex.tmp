\hypertarget{_gsgp_cuda_8cpp}{}\doxysection{D\+:/krave/\+Descargas/cudagsgp-\/master/cudagsgp-\/master/\+Gsgp\+Cuda.cpp File Reference}
\label{_gsgp_cuda_8cpp}\index{D:/krave/Descargas/cudagsgp-\/master/cudagsgp-\/master/GsgpCuda.cpp@{D:/krave/Descargas/cudagsgp-\/master/cudagsgp-\/master/GsgpCuda.cpp}}


file containing the definition of the modules (kernels) used to create the population of individuals, evaluate them, the search operator and read data  


{\ttfamily \#include \char`\"{}Gsgp\+Cuda.\+h\char`\"{}}\newline
Include dependency graph for Gsgp\+Cuda.\+cpp\+:
% FIG 0
This graph shows which files directly or indirectly include this file\+:
% FIG 1
\doxysubsection*{Functions}
\begin{DoxyCompactItemize}
\item 
const std\+::string \mbox{\hyperlink{_gsgp_cuda_8cpp_ac919c3950b37172c5ef02a12d27254b4}{current\+Date\+Time}} ()
\begin{DoxyCompactList}\small\item\em function to capture the date and time of the host, this allows to define the exact moment of each G\+S\+G\+P-\/\+C\+U\+DA run, this allows us to name the output files by date and time. \end{DoxyCompactList}\item 
void \mbox{\hyperlink{_gsgp_cuda_8cpp_a0d1515a59003b1a58eedefa6dd17a995}{cuda\+Error\+Check}} (const char $\ast$function\+Name)
\begin{DoxyCompactList}\small\item\em This function catches the error detected by the compiler and prints the user-\/friendly error message. \end{DoxyCompactList}\item 
\+\_\+\+\_\+global\+\_\+\+\_\+ void \mbox{\hyperlink{_gsgp_cuda_8cpp_a983dfdd69760a88100bc9efb04e4ac5a}{init}} (unsigned int seed, curand\+State\+\_\+t $\ast$states)
\begin{DoxyCompactList}\small\item\em This kernel is used to initialize the random states to generate random numbers with a different pseudo sequence in each thread. \end{DoxyCompactList}\item 
\+\_\+\+\_\+device\+\_\+\+\_\+ int \mbox{\hyperlink{_gsgp_cuda_8cpp_a8cde9b3f9b14f608e5f41be703a0eac4}{push}} (float val, int $\ast$push\+Genes, float $\ast$stack\+Ind)
\begin{DoxyCompactList}\small\item\em \mbox{\hyperlink{_gsgp_cuda_8cpp_a8cde9b3f9b14f608e5f41be703a0eac4}{push()}} function is used to insert an element at the top of the stack. The element is added to the stack container and the size of the stack is increased by 1. \end{DoxyCompactList}\item 
\+\_\+\+\_\+device\+\_\+\+\_\+ float \mbox{\hyperlink{_gsgp_cuda_8cpp_aeb41d1035545c56d8f6b46b7fd502e60}{pop}} (int $\ast$push\+Genes, float $\ast$stack\+Ind)
\begin{DoxyCompactList}\small\item\em \mbox{\hyperlink{_gsgp_cuda_8cpp_aeb41d1035545c56d8f6b46b7fd502e60}{pop()}} function is used to remove an element from the top of the stack(newest element in the stack). The element is removed to the stack container and the size of the stack is decreased by 1. \end{DoxyCompactList}\item 
\+\_\+\+\_\+device\+\_\+\+\_\+ bool \mbox{\hyperlink{_gsgp_cuda_8cpp_a4e8f2bcb18e95d69010cc9f82101e135}{is\+Empty}} (int $\ast$push\+Genes, unsigned int size\+Max\+Depth\+Individual)
\begin{DoxyCompactList}\small\item\em Check if a stack is empty. \end{DoxyCompactList}\item 
\+\_\+\+\_\+device\+\_\+\+\_\+ void \mbox{\hyperlink{_gsgp_cuda_8cpp_a0c1ed2f842c6b8cf261a6c9f2edd9b0c}{clear\+Stack}} (int $\ast$push\+Genes, unsigned int size\+Max\+Depth\+Individual, float $\ast$stack\+Ind)
\begin{DoxyCompactList}\small\item\em remove all elements from the stack so that in the next evaluations there are no previous values of other individuals \end{DoxyCompactList}\item 
\+\_\+\+\_\+global\+\_\+\+\_\+ void \mbox{\hyperlink{_gsgp_cuda_8cpp_af1acc65c5e1f4db666357c3565102751}{initialize\+Population}} (float $\ast$d\+Initial\+Population, int nvar, int size\+Max\+Depth\+Individual, curand\+State\+\_\+t $\ast$states, int max\+Random\+Constant)
\begin{DoxyCompactList}\small\item\em The initialize\+Population kernel creates the population of programs T and the set of random trees R uses by the G\+SM kernel, based on the desired population size and maximun program length. The individuals are representd using a linear genome, composed of valid terminals (inputs to the program) and functions (basic elements with which programs can be built). \end{DoxyCompactList}\item 
\+\_\+\+\_\+global\+\_\+\+\_\+ void \mbox{\hyperlink{_gsgp_cuda_8cpp_a9db7a327b13d8851fe8cfebeee656350}{compute\+Semantics}} (float $\ast$input\+Population, float $\ast$out\+Semantic, unsigned int size\+Max\+Depth\+Individual, float $\ast$data, int nrow, int nvar, int $\ast$push\+Genes, float $\ast$stack\+Ind)
\begin{DoxyCompactList}\small\item\em The Compute\+Semantics kernel is an interpreter, that decodes each individual and evaluates it over all fitness cases, producing as output the semantic vector of each individual. The chromosome is interpreted linearly, using an auxiliary L\+I\+FO stack D that stores terminals from the chromosome and the output from valid operations. \end{DoxyCompactList}\item 
\+\_\+\+\_\+global\+\_\+\+\_\+ void \mbox{\hyperlink{_gsgp_cuda_8cpp_ac3fa3e601ac3e2c6a64e5c45beaccd89}{compute\+Error}} (float $\ast$semantics, float $\ast$target\+Values, float $\ast$fit, int nrow)
\begin{DoxyCompactList}\small\item\em The compute\+Error kernel computes the R\+M\+SE between each row of the semantic matrix ST,m×n and the target vector t, computing the fitness of each individual in the population. \end{DoxyCompactList}\item 
\+\_\+\+\_\+device\+\_\+\+\_\+ float \mbox{\hyperlink{_gsgp_cuda_8cpp_a86a361f9f62d54f600cebb5612b660fc}{sigmoid}} (float n)
\begin{DoxyCompactList}\small\item\em auxiliary function for the geometric semantic mutation operation \end{DoxyCompactList}\item 
\+\_\+\+\_\+global\+\_\+\+\_\+ void \mbox{\hyperlink{_gsgp_cuda_8cpp_ae218d64b1ba67bb484c22c4c6ff293f5}{initialize\+Index\+Random\+Trees}} (int size\+Population, float $\ast$index\+Random\+Trees, curand\+State\+\_\+t $\ast$states)
\item 
\+\_\+\+\_\+global\+\_\+\+\_\+ void \mbox{\hyperlink{_gsgp_cuda_8cpp_a4356c2760aa10738e78745a4e2507f51}{geometric\+Semantic\+Mutation}} (float $\ast$initial\+Population\+Semantics, float $\ast$random\+Trees\+Semantics, float $\ast$new\+Semantics\+Offsprings, int size\+Population, int nrow, int t\+Elements, int generation, float $\ast$index\+Random\+Trees, \mbox{\hyperlink{structentry__}{entry\+\_\+}} $\ast$x)
\begin{DoxyCompactList}\small\item\em The G\+SM operator is basically a vector addition operation, that can be performed independently for each semantic element S\+Ti,j. However, it is necessary to select the semantics of two random trees R u and R v , and a random mutation step ms. \end{DoxyCompactList}\item 
\+\_\+\+\_\+host\+\_\+\+\_\+ void \mbox{\hyperlink{_gsgp_cuda_8cpp_a5d5d8a921c93234cad485e5fcfee6b34}{save\+Trace}} (\mbox{\hyperlink{_gsgp_cuda_8h_a9d6caa62b0d171762992c07785d6f197}{entry}} $\ast$struct\+Survivor, int generation)
\begin{DoxyCompactList}\small\item\em Function that stores the information related to the evolutionary cycle and stores the indices of the individuals that were used in each generation to create new offspring, to later perform the reconstruction of the optimal solution in the \mbox{\hyperlink{trace_8txt}{trace.\+txt}} file. \end{DoxyCompactList}\item 
\+\_\+\+\_\+host\+\_\+\+\_\+ void \mbox{\hyperlink{_gsgp_cuda_8cpp_a14bc95f02546f2ed56ce69770053714b}{read\+Inpu\+Data}} (char $\ast$train\+\_\+file, char $\ast$test\+\_\+file, float $\ast$data\+Train, float $\ast$data\+Test, float $\ast$data\+Train\+Target, float $\ast$data\+Test\+Target, int nrow, int nvar, int nrow\+Test, int nvar\+Test)
\begin{DoxyCompactList}\small\item\em function that reads data from training and test files, also reads target values to store them in pointer vectors. \end{DoxyCompactList}\item 
\+\_\+\+\_\+host\+\_\+\+\_\+ void \mbox{\hyperlink{_gsgp_cuda_8cpp_a3ad4f51591d12acf0b3d3fc0badfca4d}{read\+Config\+File}} (\mbox{\hyperlink{_gsgp_cuda_8h_a69c02f6d66c6461c9a1c6d3d98075b5d}{cfg}} $\ast$\mbox{\hyperlink{_gsgp_cuda_8h_ac6f73ac686ef53a1065b100d1f764641}{config}})
\begin{DoxyCompactList}\small\item\em Function that reads the configuration file where the parameters are found to initialize the algorithm. \end{DoxyCompactList}\end{DoxyCompactItemize}


\doxysubsection{Detailed Description}
file containing the definition of the modules (kernels) used to create the population of individuals, evaluate them, the search operator and read data 

\begin{DoxyDate}{Date}
created on 25/01/2020
\end{DoxyDate}
~\newline
 

\doxysubsection{Function Documentation}
\mbox{\Hypertarget{_gsgp_cuda_8cpp_a0c1ed2f842c6b8cf261a6c9f2edd9b0c}\label{_gsgp_cuda_8cpp_a0c1ed2f842c6b8cf261a6c9f2edd9b0c}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!clearStack@{clearStack}}
\index{clearStack@{clearStack}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{clearStack()}{clearStack()}}
{\footnotesize\ttfamily $<$ strong $>$ device$<$/strong $>$ void clear\+Stack (\begin{DoxyParamCaption}\item[{int $\ast$}]{push\+Genes,  }\item[{unsigned int}]{size\+Max\+Depth\+Individual,  }\item[{float $\ast$}]{stack\+Ind }\end{DoxyParamCaption})}



remove all elements from the stack so that in the next evaluations there are no previous values of other individuals 


\begin{DoxyParams}{Parameters}
{\em int} & $\ast$push\+Genes\+: auxiliary pointer that stores the positions of individuals \\
\hline
{\em int} & size\+Max\+Depth\+Individual\+: variable thar stores maximum depth for individuals \\
\hline
{\em float} & $\ast$stack\+Ind\+: auxiliary pointer that stores the values ​​resulting from the evaluation of each individual \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
void 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 143 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_ac3fa3e601ac3e2c6a64e5c45beaccd89}\label{_gsgp_cuda_8cpp_ac3fa3e601ac3e2c6a64e5c45beaccd89}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!computeError@{computeError}}
\index{computeError@{computeError}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{computeError()}{computeError()}}
{\footnotesize\ttfamily $<$ strong $>$ global$<$/strong $>$ void compute\+Error (\begin{DoxyParamCaption}\item[{float $\ast$}]{semantics,  }\item[{float $\ast$}]{target\+Values,  }\item[{float $\ast$}]{fit,  }\item[{int}]{nrow }\end{DoxyParamCaption})}



The compute\+Error kernel computes the R\+M\+SE between each row of the semantic matrix ST,m×n and the target vector t, computing the fitness of each individual in the population. 


\begin{DoxyParams}{Parameters}
{\em float} & $\ast$semantics\+: vector of pointers that contains the semantics of the individuals of the initial population \\
\hline
{\em float} & $\ast$target\+Values\+: pointer containing the target values of train or test \\
\hline
{\em float} & $\ast$fit\+: vector that will store the error of each individual in the population \\
\hline
{\em int} & nrow\+: variable containing the number of rows (instances) of the training and test dataset \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
void 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 324 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_a9db7a327b13d8851fe8cfebeee656350}\label{_gsgp_cuda_8cpp_a9db7a327b13d8851fe8cfebeee656350}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!computeSemantics@{computeSemantics}}
\index{computeSemantics@{computeSemantics}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{computeSemantics()}{computeSemantics()}}
{\footnotesize\ttfamily $<$ strong $>$ global$<$/strong $>$ void compute\+Semantics (\begin{DoxyParamCaption}\item[{float $\ast$}]{input\+Population,  }\item[{float $\ast$}]{out\+Semantic,  }\item[{unsigned int}]{size\+Max\+Depth\+Individual,  }\item[{float $\ast$}]{data,  }\item[{int}]{nrow,  }\item[{int}]{nvar,  }\item[{int $\ast$}]{push\+Genes,  }\item[{float $\ast$}]{stack\+Ind }\end{DoxyParamCaption})}



The Compute\+Semantics kernel is an interpreter, that decodes each individual and evaluates it over all fitness cases, producing as output the semantic vector of each individual. The chromosome is interpreted linearly, using an auxiliary L\+I\+FO stack D that stores terminals from the chromosome and the output from valid operations. 

~\newline
 
\begin{DoxyParams}{Parameters}
{\em float} & $\ast$input\+Population\+: vector pointers to store the individuals of the population \\
\hline
{\em float} & $\ast$out\+Semantic\+: vector pointers to store the semantics of each individual in the population \\
\hline
{\em int} & size\+Max\+Depth\+Individual\+: variable thar stores maximum depth for individuals \\
\hline
{\em float} & $\ast$data\+: pointer vector containing training or test data \\
\hline
{\em int} & nrow\+: variable containing the number of rows (instances) of the training dataset \\
\hline
{\em int} & nvar\+: variable containing the number of columns (excluding the target) of the training dataset \\
\hline
{\em int} & $\ast$push\+Genes\+: auxiliary pointer that stores the positions of individuals \\
\hline
{\em float} & $\ast$stack\+Ind\+: auxiliary pointer that stores the values ​​resulting from the interpretation of each individual \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
void 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 207 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_a0d1515a59003b1a58eedefa6dd17a995}\label{_gsgp_cuda_8cpp_a0d1515a59003b1a58eedefa6dd17a995}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!cudaErrorCheck@{cudaErrorCheck}}
\index{cudaErrorCheck@{cudaErrorCheck}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{cudaErrorCheck()}{cudaErrorCheck()}}
{\footnotesize\ttfamily void cuda\+Error\+Check (\begin{DoxyParamCaption}\item[{const char $\ast$}]{function\+Name }\end{DoxyParamCaption})}



This function catches the error detected by the compiler and prints the user-\/friendly error message. 


\begin{DoxyParams}{Parameters}
{\em char} & funtion\+Name\+: pointer to the name of the kernel executed to verify if there was an error \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
void 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 52 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_ac919c3950b37172c5ef02a12d27254b4}\label{_gsgp_cuda_8cpp_ac919c3950b37172c5ef02a12d27254b4}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!currentDateTime@{currentDateTime}}
\index{currentDateTime@{currentDateTime}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{currentDateTime()}{currentDateTime()}}
{\footnotesize\ttfamily string current\+Date\+Time (\begin{DoxyParamCaption}{ }\end{DoxyParamCaption})}



function to capture the date and time of the host, this allows to define the exact moment of each G\+S\+G\+P-\/\+C\+U\+DA run, this allows us to name the output files by date and time. 

\begin{DoxyReturn}{Returns}
char\+: return date and time from the host computer 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
25/01/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 33 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_a4356c2760aa10738e78745a4e2507f51}\label{_gsgp_cuda_8cpp_a4356c2760aa10738e78745a4e2507f51}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!geometricSemanticMutation@{geometricSemanticMutation}}
\index{geometricSemanticMutation@{geometricSemanticMutation}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{geometricSemanticMutation()}{geometricSemanticMutation()}}
{\footnotesize\ttfamily $<$ strong $>$ global$<$/strong $>$ void geometric\+Semantic\+Mutation (\begin{DoxyParamCaption}\item[{float $\ast$}]{initial\+Population\+Semantics,  }\item[{float $\ast$}]{random\+Trees\+Semantics,  }\item[{float $\ast$}]{new\+Semantics\+Offsprings,  }\item[{int}]{size\+Population,  }\item[{int}]{nrow,  }\item[{int}]{t\+Elements,  }\item[{int}]{generation,  }\item[{float $\ast$}]{index\+Random\+Trees,  }\item[{\mbox{\hyperlink{structentry__}{entry\+\_\+}} $\ast$}]{x }\end{DoxyParamCaption})}



The G\+SM operator is basically a vector addition operation, that can be performed independently for each semantic element S\+Ti,j. However, it is necessary to select the semantics of two random trees R u and R v , and a random mutation step ms. 


\begin{DoxyParams}{Parameters}
{\em float} & $\ast$initial\+Population\+Semantics\+: this vector of pointers contains the semantics of the initial population \\
\hline
{\em float} & $\ast$random\+Trees\+Semantics\+: this vector of pointers contains the semantics of the random trees \\
\hline
{\em float} & $\ast$new\+Semantics\+Offsprings\+: this vector of pointers will store the semantics of the new offspring \\
\hline
{\em int} & size\+Population\+: this variable contains the number of individuals that the population has \\
\hline
{\em int} & nrow\+: variable containing the number of rows (instances) of the training dataset \\
\hline
{\em int} & t\+Elements\+: variables containing the total number of semantic elements \\
\hline
{\em int} & generation\+: number of generation \\
\hline
{\em float} & $\ast$index\+Random\+Trees\+: this pointer stores the indexes randomly for mutation \\
\hline
{\em struc} & $\ast$x\+: variable used to store training and test instances \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
void 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
\end{DoxyAuthor}

\begin{DoxyParams}{Parameters}
{\em float} & $\ast$initial\+Population\+Semantics\+: this vector of pointers contains the semantics of the initial population \\
\hline
{\em float} & $\ast$random\+Trees\+Semantics\+: this vector of pointers contains the semantics of the random trees \\
\hline
{\em float} & $\ast$new\+Semantics\+Offsprings\+: this vector of pointers will store the semantics of the new offspring \\
\hline
{\em int} & size\+Population\+: this variable contains the number of individuals that the population has \\
\hline
{\em int} & nrow\+: variable containing the numbers of rows (instances) of the training dataset \\
\hline
{\em int} & t\+Elements\+: variables containing the total number of semantic elements \\
\hline
{\em int} & generation\+: number of generation \\
\hline
{\em float} & $\ast$index\+Random\+Trees\+: this pointer stores the indexes randomly for mutation \\
\hline
{\em struc} & $\ast$x\+: variable used to store training and test instances \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
void 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 385 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_a983dfdd69760a88100bc9efb04e4ac5a}\label{_gsgp_cuda_8cpp_a983dfdd69760a88100bc9efb04e4ac5a}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!init@{init}}
\index{init@{init}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{init()}{init()}}
{\footnotesize\ttfamily $<$ strong $>$ global$<$/strong $>$ void init (\begin{DoxyParamCaption}\item[{unsigned int}]{seed,  }\item[{curand\+State\+\_\+t $\ast$}]{states }\end{DoxyParamCaption})}



This kernel is used to initialize the random states to generate random numbers with a different pseudo sequence in each thread. 


\begin{DoxyParams}{Parameters}
{\em int} & seed\+: used to generate a random number for each core ~\newline
 \\
\hline
{\em curand\+State\+\_\+t} & states\+: pointer to store a random state for each thread \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
void 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 72 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_ae218d64b1ba67bb484c22c4c6ff293f5}\label{_gsgp_cuda_8cpp_ae218d64b1ba67bb484c22c4c6ff293f5}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!initializeIndexRandomTrees@{initializeIndexRandomTrees}}
\index{initializeIndexRandomTrees@{initializeIndexRandomTrees}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{initializeIndexRandomTrees()}{initializeIndexRandomTrees()}}
{\footnotesize\ttfamily \+\_\+\+\_\+global\+\_\+\+\_\+ void initialize\+Index\+Random\+Trees (\begin{DoxyParamCaption}\item[{int}]{size\+Population,  }\item[{float $\ast$}]{index\+Random\+Trees,  }\item[{curand\+State\+\_\+t $\ast$}]{states }\end{DoxyParamCaption})}



Definition at line 361 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_af1acc65c5e1f4db666357c3565102751}\label{_gsgp_cuda_8cpp_af1acc65c5e1f4db666357c3565102751}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!initializePopulation@{initializePopulation}}
\index{initializePopulation@{initializePopulation}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{initializePopulation()}{initializePopulation()}}
{\footnotesize\ttfamily $<$ strong $>$ global$<$/strong $>$ void initialize\+Population (\begin{DoxyParamCaption}\item[{float $\ast$}]{d\+Initial\+Population,  }\item[{int}]{nvar,  }\item[{int}]{size\+Max\+Depth\+Individual,  }\item[{curand\+State\+\_\+t $\ast$}]{states,  }\item[{int}]{max\+Random\+Constant }\end{DoxyParamCaption})}



The initialize\+Population kernel creates the population of programs T and the set of random trees R uses by the G\+SM kernel, based on the desired population size and maximun program length. The individuals are representd using a linear genome, composed of valid terminals (inputs to the program) and functions (basic elements with which programs can be built). 


\begin{DoxyParams}{Parameters}
{\em float} & $\ast$d\+Initial\+Population\+: vector pointers to store the individuals of the initial population \\
\hline
{\em int} & nvar\+: variable containing the number of columns (excluding the target) of the training dataset \\
\hline
{\em int} & size\+Max\+Depth\+Individual\+: variable thar stores maximum depth for individuals \\
\hline
{\em curand\+State\+\_\+t} & $\ast$states\+: random status pointer to generate random numbers for each thread \\
\hline
{\em int} & max\+Random\+Constant\+: variable containing the maximum number to generate ephemeral constants \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
void 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 166 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_a4e8f2bcb18e95d69010cc9f82101e135}\label{_gsgp_cuda_8cpp_a4e8f2bcb18e95d69010cc9f82101e135}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!isEmpty@{isEmpty}}
\index{isEmpty@{isEmpty}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{isEmpty()}{isEmpty()}}
{\footnotesize\ttfamily $<$ strong $>$ device$<$/strong $>$ bool is\+Empty (\begin{DoxyParamCaption}\item[{int $\ast$}]{push\+Genes,  }\item[{unsigned int}]{size\+Max\+Depth\+Individual }\end{DoxyParamCaption})}



Check if a stack is empty. 

~\newline
 
\begin{DoxyParams}{Parameters}
{\em int} & $\ast$push\+Genes\+: auxiliary pointer that stores the positions of individuals \\
\hline
{\em int} & size\+Max\+Depth\+Individual\+: variable thar stores maximum depth for individuals \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
bool -\/ true if the stack is empty, false otherwise 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 123 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_aeb41d1035545c56d8f6b46b7fd502e60}\label{_gsgp_cuda_8cpp_aeb41d1035545c56d8f6b46b7fd502e60}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!pop@{pop}}
\index{pop@{pop}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{pop()}{pop()}}
{\footnotesize\ttfamily $<$ strong $>$ device$<$/strong $>$ float pop (\begin{DoxyParamCaption}\item[{int $\ast$}]{push\+Genes,  }\item[{float $\ast$}]{stack\+Ind }\end{DoxyParamCaption})}



\mbox{\hyperlink{_gsgp_cuda_8cpp_aeb41d1035545c56d8f6b46b7fd502e60}{pop()}} function is used to remove an element from the top of the stack(newest element in the stack). The element is removed to the stack container and the size of the stack is decreased by 1. 


\begin{DoxyParams}{Parameters}
{\em int} & $\ast$push\+Genes\+: auxiliary pointer that stores the positions of individuals \\
\hline
{\em float} & $\ast$stack\+Ind\+: auxiliary pointer that stores the values ​​resulting from the interpretation of each individual \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
float\+: returns the stack\+Ind without the value positioned in push\+Genes \mbox{[}tid\mbox{]} 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 106 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_a8cde9b3f9b14f608e5f41be703a0eac4}\label{_gsgp_cuda_8cpp_a8cde9b3f9b14f608e5f41be703a0eac4}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!push@{push}}
\index{push@{push}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{push()}{push()}}
{\footnotesize\ttfamily $<$ strong $>$ device$<$/strong $>$ int push (\begin{DoxyParamCaption}\item[{float}]{val,  }\item[{int $\ast$}]{push\+Genes,  }\item[{float $\ast$}]{stack\+Ind }\end{DoxyParamCaption})}



\mbox{\hyperlink{_gsgp_cuda_8cpp_a8cde9b3f9b14f608e5f41be703a0eac4}{push()}} function is used to insert an element at the top of the stack. The element is added to the stack container and the size of the stack is increased by 1. 


\begin{DoxyParams}{Parameters}
{\em float} & val\+: variable that stores a value resulting from a valid operation in the interpreter \\
\hline
{\em int} & $\ast$push\+Genes\+: auxiliary pointer that stores the positions of individuals \\
\hline
{\em float} & $\ast$stack\+Ind\+: auxiliary pointer that stores the values ​​resulting from the interpretation of each individual \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
int\+: auxiliary pointer that stores the positions of individuals + 1 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 89 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_a3ad4f51591d12acf0b3d3fc0badfca4d}\label{_gsgp_cuda_8cpp_a3ad4f51591d12acf0b3d3fc0badfca4d}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!readConfigFile@{readConfigFile}}
\index{readConfigFile@{readConfigFile}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{readConfigFile()}{readConfigFile()}}
{\footnotesize\ttfamily $<$ strong $>$ host$<$/strong $>$ void read\+Config\+File (\begin{DoxyParamCaption}\item[{\mbox{\hyperlink{_gsgp_cuda_8h_a69c02f6d66c6461c9a1c6d3d98075b5d}{cfg}} $\ast$}]{config }\end{DoxyParamCaption})}



Function that reads the configuration file where the parameters are found to initialize the algorithm. 


\begin{DoxyParams}{Parameters}
{\em cfg} & $\ast$config\+: pointer to the struct containing the variables needed to run the program \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
void 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 504 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_a14bc95f02546f2ed56ce69770053714b}\label{_gsgp_cuda_8cpp_a14bc95f02546f2ed56ce69770053714b}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!readInpuData@{readInpuData}}
\index{readInpuData@{readInpuData}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{readInpuData()}{readInpuData()}}
{\footnotesize\ttfamily $<$ strong $>$ host$<$/strong $>$ void read\+Inpu\+Data (\begin{DoxyParamCaption}\item[{char $\ast$}]{train\+\_\+file,  }\item[{char $\ast$}]{test\+\_\+file,  }\item[{float $\ast$}]{data\+Train,  }\item[{float $\ast$}]{data\+Test,  }\item[{float $\ast$}]{data\+Train\+Target,  }\item[{float $\ast$}]{data\+Test\+Target,  }\item[{int}]{nrow,  }\item[{int}]{nvar,  }\item[{int}]{nrow\+Test,  }\item[{int}]{nvar\+Test }\end{DoxyParamCaption})}



function that reads data from training and test files, also reads target values to store them in pointer vectors. 


\begin{DoxyParams}{Parameters}
{\em char} & $\ast$train\+\_\+file\+: name of the file with training instances \\
\hline
{\em char} & $\ast$test\+\_\+file\+: name of the file with test instances \\
\hline
{\em float} & $\ast$data\+Train\+: vector pointers to store training data \\
\hline
{\em float} & $\ast$data\+Test\+: vector pointers to store test data \\
\hline
{\em float} & $\ast$data\+Train\+Target\+: vector pointers to store training target data \\
\hline
{\em float} & $\ast$data\+Test\+Target\+: vector pointers to store test target data \\
\hline
{\em int} & nrow\+: variable containing the number of rows (instances) of the training dataset \\
\hline
{\em int} & nvar\+: variable containing the number of columns (excluding the target) of the training dataset \\
\hline
{\em int} & nrow\+Test\+: variable containing the number of rows (instances) of the test dataset \\
\hline
{\em int} & nvar\+Test\+: variable containing the number of columns (excluding the target) of the test dataset \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
void\+: 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 442 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_a5d5d8a921c93234cad485e5fcfee6b34}\label{_gsgp_cuda_8cpp_a5d5d8a921c93234cad485e5fcfee6b34}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!saveTrace@{saveTrace}}
\index{saveTrace@{saveTrace}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{saveTrace()}{saveTrace()}}
{\footnotesize\ttfamily $<$ strong $>$ host$<$/strong $>$ void save\+Trace (\begin{DoxyParamCaption}\item[{\mbox{\hyperlink{_gsgp_cuda_8h_a9d6caa62b0d171762992c07785d6f197}{entry}} $\ast$}]{struct\+Survivor,  }\item[{int}]{generation }\end{DoxyParamCaption})}



Function that stores the information related to the evolutionary cycle and stores the indices of the individuals that were used in each generation to create new offspring, to later perform the reconstruction of the optimal solution in the \mbox{\hyperlink{trace_8txt}{trace.\+txt}} file. 


\begin{DoxyParams}{Parameters}
{\em struc} & $\ast$struct\+Survivor\+: pointer that stores information of the best individual throughout the generations \\
\hline
{\em int} & generation\+: number of generations \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
void 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 412 of file Gsgp\+Cuda.\+cpp.

\mbox{\Hypertarget{_gsgp_cuda_8cpp_a86a361f9f62d54f600cebb5612b660fc}\label{_gsgp_cuda_8cpp_a86a361f9f62d54f600cebb5612b660fc}} 
\index{GsgpCuda.cpp@{GsgpCuda.cpp}!sigmoid@{sigmoid}}
\index{sigmoid@{sigmoid}!GsgpCuda.cpp@{GsgpCuda.cpp}}
\doxysubsubsection{\texorpdfstring{sigmoid()}{sigmoid()}}
{\footnotesize\ttfamily $<$ strong $>$ device$<$/strong $>$ float sigmoid (\begin{DoxyParamCaption}\item[{float}]{n }\end{DoxyParamCaption})}



auxiliary function for the geometric semantic mutation operation 


\begin{DoxyParams}{Parameters}
{\em float} & n\+: semantic value of a random tree \\
\hline
\end{DoxyParams}
\begin{DoxyReturn}{Returns}
float n\+: value resulting from the function 
\end{DoxyReturn}
\begin{DoxyDate}{Date}
01/25/2020 
\end{DoxyDate}
\begin{DoxyAuthor}{Author}
José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith 
\end{DoxyAuthor}


Definition at line 345 of file Gsgp\+Cuda.\+cpp.


/*<one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2020 José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

     This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//! \file   GsgpCuda.cu
//! \brief  file containing the main with the geometric semantic genetic programming algorithm
//! \date   created on 25/01/2020

#include "GsgpCuda.cpp"
using namespace std;   

/*!
* \fn       int main(int argc, const char **argv)
* \brief    main method that runs the GSGP algorithm
* \param    int argc: number of parameters of the program
* \param    const char **argv: array of strings that contains the parameters of the program
* \return   int: 0 if the program ends without errors
* \date     25/01/2020
* \author   Jose Manuel Muñoz Contreras
* \file     GsgpCuda.cu
*/

int main(int argc, char **argv){
    
    srand(time(NULL)); /*!< initialization of the seed for the generation of random numbers*/

    cudaSetDevice(0); /*!< select a GPU device*/

    printf("\n Starting GsgpCUDA \n\n");
    
    readConfigFile(&config); /*!< reading the parameters of the algorithm */
    
    const int sizeMaxDepthIndividual = (int)exp2(config.maxDepth*1.0) - 1; /*!< variable that stores maximum depth for individuals */
    
    int sizeMemPopulation = sizeof(float) * config.populationSize * sizeMaxDepthIndividual; /*!< variable that stores size in bytes for initial population*/
    
    int twoSizeMemPopulation = sizeof(float) * (config.populationSize*2); /*!< variable that stores twice the size in bytes of an initial population to store random numbers*/
    
    int sizeMemIndividuals = sizeof(float) * config.populationSize; /*!< variable that stores size in bytes of the number of individuals in the initial population*/
    
    long int twoSizePopulation = (config.populationSize*2); /*!< variable storing twice the initial population of individuals to generate random positions*/
    
    long int sizeMemSemanticTrain = sizeof(float)*(config.populationSize*config.nrow); /*!< variable that stores the size in bytes of semantics for the entire population with training data*/
    
    long int sizeMemSemanticTest = sizeof(float)*(config.populationSize*config.nrowTest); /*!< variable that stores the size in bytes of semantics for the entire population with test data*/
    
    long int sizeMemDataTrain = sizeof(float)*(config.nrow*config.nvar); /*!< variable that stores the size in bytes the size of the training data*/
    
    long int sizeMemDataTest = sizeof(float)*(config.nrowTest*config.nvarTest); /*!< variable that stores the size in bytes the size of the test data*/
    
    long int sizeElementsSemanticTrain = (config.populationSize*config.nrow); /*!< variable that stores training data elements*/
    
    long int sizeElementsSemanticTest = (config.populationSize*config.nrowTest); /*!< variable that stores test data elements*/
    
    size_t structMemMutation = (sizeof(entry_)*config.populationSize); /*!< variable that stores the size in bytes of the structure to store the mutation record*/
    
    size_t structMemSurvivor = (sizeof(entry_)*config.maxNumberGenerations); /*!< variable that stores the size in bytes of the structure to store the survival record*/
    
    int gridSize,minGridSize,blockSize; /*!< variables that store the execution configuration for a kernel in the GPU*/
    
    int gridSizeTest,minGridSizeTest,blockSizeTest; /*!< variables that store the execution configuration for a kernel in the GPU*/
    
    std::string timeExecution1 = "run_"; /*!< variable name structure responsible for indicating the run*/
    
    std::string timeExecution2 = ".csv"; /*!< variable name structure responsible for indicating the file extension*/
    
    std::string dateTime =currentDateTime(); /*!< variable name structure responsible for capturing the date and time of the run*/
    
    timeExecution1 = timeExecution1 + dateTime + timeExecution2; /*!< variable that stores file name matching*/
    
    std::ofstream times(timeExecution1,ios::out); /*!< pointer to the timeExecution1 file that contains the time consumed by the different algorithm modules*/
    
    cudaEvent_t startRun, stopRun; /*!< Variable used to create a start mark and a stop mark to create events*/
    
    cudaEventCreate(&startRun); /*!< function that initializes the start event*/
    
    cudaEventCreate(&stopRun); /*!< function that initializes the stop event*/
    
    float executionTime = 0, initialitionTimePopulation = 0, timeComputeSemantics = 0, generationTime = 0; /*!< variables that store the time in milliseconds between the events mark1 and mark2.*/
    
    curandState_t* states; /*!< CUDA's random number library uses curandState_t to keep track of the seed value we will store a random state for every thread*/
    
    cudaMalloc((void**) &states, config.populationSize * sizeof(curandState_t)); /*!< allocate space on the GPU for the random states*/
    
    cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSize, init, 0, config.populationSize); /*!< heuristic function used to choose a good block size is to aim at high occupancy*/
    
    gridSize = (config.populationSize + blockSize - 1) / blockSize; /*!< round up according to array size*/
    
    init<<<gridSize, blockSize>>>(time(0), states); /*!< invoke the GPU to initialize all of the random states*/
    
    /*!< algorithm run cycle*/
    for (int runs_ = 1; runs_ <= config.numberRuns; runs_++)
    {          
       cudaEventRecord(startRun);     
       std::string fitnessTrain  = "run_fitnestrain";
       std::string fitnessTrain2 = ".csv";
       std::string fitnessTrain3 = currentDateTime();
       fitnessTrain = fitnessTrain + fitnessTrain3 + fitnessTrain2;
       
       std::ofstream fitTraining(fitnessTrain,ios::out); /*!< pointer to the file fitnesstrain.csv containing the training fitness of the best individual at each generation*/
             
       std::string fitnessTest  = "run_fitnestest";
       std::string fitnessTest2 = ".csv";
       std::string fitnessTest3 = currentDateTime();
       fitnessTest = fitnessTest + fitnessTest3 +fitnessTest2;
       
       std::ofstream fitTesting(fitnessTest,ios::out); /*!< pointer to the file fitnesstest.csv containing the test fitness of the best individual at each generation*/

       
       cublasHandle_t handle; /*!< the handle to the cuBLAS library context*/
       
       cublasCreate(&handle); /*!< initialized using the function and is explicitly passed to every subsequent library function call*/

       float *dInitialPopulation,*dRandomTrees,*hInitialPopulation;  /*!< This block contains the vectors of pointers to store the population and random trees and space allocation in the GPU*/
       hInitialPopulation = (float *)malloc(sizeMemPopulation); 
       checkCudaErrors(cudaMalloc((void **)&dRandomTrees, sizeMemPopulation)); 
       checkCudaErrors(cudaMalloc((void **)&dInitialPopulation, sizeMemPopulation));
       
       
       entry  *dStructMutation,*dStructSurvivor; /*!< This block contains the vectors of pointers to store the structure to keep track of mutation and survival and space allocation in the GPU*/
       checkCudaErrors(cudaMallocManaged(&dStructMutation,structMemMutation)); 
       checkCudaErrors(cudaMallocManaged(&dStructSurvivor,structMemSurvivor));
       
       
       char pathTrain[50]=""; /*!< name of the file with training instances */
       
       char pathTest[50]="";  /*!< name of the file with test instances*/
       
       /*!< reading the names of the input files */
       for (int i=1; i<argc-1; i++)
        {
           if(strncmp(argv[i], "-train_file", 11) == 0)
            {
                strcat(pathTrain,argv[++i]);
            }else
            {
                if(strncmp(argv[i], "-test_file", 10) == 0)
                {
                    strcat(pathTest,argv[++i]);
                }
            }
        }

       
        float *uDataTrain, *uDataTest, *uDataTrainTarget, *uDataTestTarget;  /*!< this block contains the pointer of vectors for the input data and target values ​​and assignment in the GPU*/
        checkCudaErrors(cudaMallocManaged(&uDataTrain, sizeMemDataTrain));
        checkCudaErrors(cudaMallocManaged(&uDataTest, sizeMemDataTest));      
        checkCudaErrors(cudaMallocManaged(&uDataTrainTarget, sizeof(float)*config.nrow));
        checkCudaErrors(cudaMallocManaged(&uDataTestTarget, sizeof(float)*config.nrowTest));

        
        float *uFit, *uFitTest; /*!< pointers of vectors of training and test fitness values at generation g and assignment in the GPU*/
        checkCudaErrors(cudaMallocManaged(&uFit, sizeMemIndividuals));
        checkCudaErrors(cudaMallocManaged(&uFitTest, sizeMemIndividuals));

        
        float *uSemanticTrainCases, *uSemanticTestCases, *uSemanticRandomTrees, *uSemanticTestRandomTrees; /*!< pointer of vectors that contain the semantics of an individual in the population, calculated with the training set and test in generation g and its allocation in GPU*/
        checkCudaErrors(cudaMallocManaged(&uSemanticTrainCases,sizeMemSemanticTrain));       
        checkCudaErrors(cudaMallocManaged(&uSemanticTestCases,sizeMemSemanticTest));       
        checkCudaErrors(cudaMallocManaged(&uSemanticRandomTrees,sizeMemSemanticTrain));      
        checkCudaErrors(cudaMallocManaged(&uSemanticTestRandomTrees,sizeMemSemanticTest)); 
        
        
        float *uStackInd; /*!< auxiliary pointer vectors for the interpreter and calculate the semantics for the populations and assignment in the GPU*/
        int   *uPushGenes;
        checkCudaErrors(cudaMalloc((void**)&uPushGenes, sizeMemIndividuals));
        checkCudaErrors(cudaMalloc((void**)&uStackInd, sizeMemPopulation));
        
        float *tempSemantic,*tempFitnes,*tempSemanticTest,*tempFitnesTest; /*!< temporal variables to perform the movement of pointers in survival*/

        readInpuData(pathTrain, pathTest, uDataTrain, uDataTest, uDataTrainTarget, uDataTestTarget, config.nrow, config.nvar, config.nrowTest, config.nvarTest); /*!< load set data train and test*/
       
        //cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSize, initializePopulation, 0, config.populationSize); /*!< heuristic function used to choose a good block size is to aim at high occupancy*/
        //gridSize = (config.populationSize + blockSize - 1) / blockSize; /*!< round up according to array size*/
        
        cudaEvent_t startInitialPop, stopInitialPop; /*!< this section declares and initializes the variables for the events and captures the time elapsed in the initialization of the initial population in the GPU*/
        cudaEventCreate(&startInitialPop);
        cudaEventCreate(&stopInitialPop);
        cudaEventRecord(startInitialPop);

        /*!< invokes the GPU to initialize the initial population*/
        initializePopulation<<< gridSize, blockSize >>>(dInitialPopulation, config.nvar, sizeMaxDepthIndividual, states, config.maxRandomConstant,4);
        cudaErrorCheck("initializePopulation");

        cudaEventRecord(stopInitialPop);
        cudaEventSynchronize(stopInitialPop);
        cudaEventElapsedTime(&initialitionTimePopulation, startInitialPop, stopInitialPop);
        cudaEventDestroy(startInitialPop);
        cudaEventDestroy(stopInitialPop);

        /*!<return the initial population of the device to the host*/
        cudaMemcpy(hInitialPopulation, dInitialPopulation, sizeMemPopulation, cudaMemcpyDeviceToHost);
        
        /*!< invokes the GPU to initialize the random trees*/
        initializePopulation<<< gridSize, blockSize >>>(dRandomTrees, config.nvar, sizeMaxDepthIndividual, states, config.maxRandomConstant,4);    
        cudaErrorCheck("initializePopulation");

        
        cudaEvent_t startComputeSemantics, stopComputeSemantics; /*!< This section declares and initializes the variables for the events and captures the time elapsed in the interpretation of the initial population in the GPU*/
        cudaEventCreate(&startComputeSemantics);
        cudaEventCreate(&stopComputeSemantics);
        cudaEventRecord(startComputeSemantics);

        cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSize, computeSemantics, 0, config.populationSize); /*!< heuristic function used to choose a good block size is to aim at high occupancy*/
    
        gridSize = (config.populationSize + blockSize - 1) / blockSize; /*!< round up according to array size*/
        
        /*!< invokes the GPU to interpret the initial population with data train*/
        computeSemantics<<< gridSize, blockSize >>>(dInitialPopulation, uSemanticTrainCases, sizeMaxDepthIndividual, uDataTrain, config.nrow, config.nvar, uPushGenes, uStackInd);
        cudaErrorCheck("computeSemantics");

        /*!< invokes the GPU to interpret the random trees with data train*/
        computeSemantics<<< gridSize, blockSize >>>(dRandomTrees, uSemanticRandomTrees, sizeMaxDepthIndividual, uDataTrain, config.nrow, config.nvar, uPushGenes, uStackInd);
        cudaErrorCheck("computeSemantics");

        cudaEventRecord(stopComputeSemantics);
        cudaEventSynchronize(stopComputeSemantics);
        cudaEventElapsedTime(&timeComputeSemantics, startComputeSemantics, stopComputeSemantics);
        cudaEventDestroy(startComputeSemantics);
        cudaEventDestroy(stopComputeSemantics);
        
        /*!< invokes the GPU to interpret the initial population with data train*/
        computeSemantics<<< gridSize, blockSize >>>(dInitialPopulation, uSemanticTestCases, sizeMaxDepthIndividual, uDataTest, config.nrowTest, config.nvarTest, uPushGenes, uStackInd);
        cudaErrorCheck("computeSemantics");

        /*!< invokes the GPU to interpret the random trees with data test*/
        computeSemantics<<< gridSize, blockSize >>>(dRandomTrees, uSemanticTestRandomTrees, sizeMaxDepthIndividual, uDataTest, config.nrowTest, config.nvarTest, uPushGenes, uStackInd);
        cudaErrorCheck("computeSemantics");

        /*!< memory is deallocated for training data and auxiliary vectors for the interpreter*/
        cudaFree(uDataTrain);
        cudaFree(uDataTest);
        cudaFree(uStackInd);
        cudaFree(uPushGenes);

        cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSize, computeError, 0, config.populationSize); 
        gridSize = (config.populationSize + blockSize - 1) / blockSize;
        
        /*!< invokes the GPU to calculate the error (RMSE) the initial population*/
        computeError<<< gridSize, blockSize >>>(uSemanticTrainCases, uDataTrainTarget, uFit, config.nrow);
        cudaErrorCheck("computeError");
        
        
        int result,incx1=1,indexBestIndividual; /*!< this section makes use of the isamin de cublas function to determine the position of the best individual*/
        cublasIsamin(handle, config.populationSize, uFit, incx1, &result);
        indexBestIndividual = result-1;

        /*!< invokes the GPU to calculate the error (RMSE) the initial population*/
        computeError<<< gridSize, blockSize >>>(uSemanticTestCases, uDataTestTarget, uFitTest, config.nrowTest);    
        cudaErrorCheck("computeError");
        
        /*!< function is necessary so that the CPU does not continue with the execution of the program and allows to capture the fitness*/
        cudaDeviceSynchronize();
        /*!< writing the  training fitness of the best individual on the file fitnesstrain.csv*/
        fitTraining << uFit[indexBestIndividual]     <<endl;
        /*!< writing the  test fitness of the best individual on the file fitnesstest.csv*/
        fitTesting  << uFitTest[indexBestIndividual] << endl;
        
        float *uSemanticTrainCasesNew, *uFitNew, *uSemanticTestCasesNew, *uFitTestNew; /*!< vectors that contain the semantics of an individual in the population, calculated in the training and test set in the g + 1 generation and its allocation in GPU*/
        checkCudaErrors(cudaMallocManaged(&uSemanticTrainCasesNew,sizeMemSemanticTrain));
        checkCudaErrors(cudaMallocManaged(&uFitNew, sizeMemPopulation));
        checkCudaErrors(cudaMallocManaged(&uSemanticTestCasesNew,sizeMemSemanticTest));
        checkCudaErrors(cudaMallocManaged(&uFitTestNew, sizeMemPopulation));

        cudaEvent_t startGsgp, stopGsgp;
        cudaEventCreate(&startGsgp);
        cudaEventCreate(&stopGsgp);
        
        curandState_t* State;
        cudaMalloc((void**) &State, (twoSizePopulation) * sizeof(curandState_t));
        cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSize, init, 0, twoSizePopulation);
        gridSize = (twoSizePopulation + blockSize - 1) / blockSize;
        init<<<gridSize, blockSize>>>(time(NULL), State);
        
        float *indexRandomTrees; /*!< vector pointers to save random positions of random trees and allocation in GPU*/
        checkCudaErrors(cudaMallocManaged(&indexRandomTrees,twoSizeMemPopulation));
        
        /*!< main GSGP cycle*/
        for ( int generation=1; generation<=config.maxNumberGenerations; generation++)
        {
            /*!< register execution time*/
            cudaEventRecord(startGsgp);

            cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSize, initializeIndexRandomTrees, 0, twoSizePopulation);
            gridSize = (twoSizePopulation + blockSize - 1) / blockSize;
            
            initializeIndexRandomTrees<<<gridSize,blockSize >>>( config.populationSize, indexRandomTrees, State );
            cudaErrorCheck("initializeIndexRandomTrees");

            cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSize, geometricSemanticMutation, 0, sizeElementsSemanticTrain); 
            gridSize = (sizeElementsSemanticTrain + blockSize - 1) / blockSize;
            /*!< geometric semantic mutation with semantic train*/
            geometricSemanticMutation<<< gridSize, blockSize >>>(uSemanticTrainCases, uSemanticRandomTrees,uSemanticTrainCasesNew,
                config.populationSize, config.nrow, sizeElementsSemanticTrain, generation, indexRandomTrees, dStructMutation);
            cudaErrorCheck("geometricSemanticMutation");

            cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSize, computeError, 0, config.populationSize); 
            gridSize = (config.populationSize + blockSize - 1) / blockSize;
            /*!< invokes the GPU to calculate the error (RMSE) the new population*/
            computeError<<< gridSize,blockSize >>>(uSemanticTrainCasesNew, uDataTrainTarget, uFitNew, config.nrow);
            cudaErrorCheck("computeError");

            /*!< this section makes use of the isamin de cublas function to determine the position of the best individual of the new population*/
            int resultBestOffspring,incxBestOffspring=1,indexBestOffspring;
            cublasIsamin(handle, config.populationSize, uFitNew, incxBestOffspring, &resultBestOffspring);
            indexBestOffspring = resultBestOffspring-1;

            /*!< this section makes use of the isamin de cublas function to determine the position of the worst individual of the new population*/
            int resultWorst,incxWorst=1,indexWorstOffspring;
            cublasIsamax(handle, config.populationSize, uFitNew, incxWorst, &resultWorst);
            indexWorstOffspring = resultWorst-1;
            
            cudaOccupancyMaxPotentialBlockSize(&minGridSizeTest, &blockSizeTest, geometricSemanticMutation, 0, sizeElementsSemanticTest); 
            gridSizeTest = (sizeElementsSemanticTest + blockSizeTest - 1) / blockSizeTest;
            /*!< geometric semantic mutation with semantic test*/
            geometricSemanticMutation<<< gridSizeTest, blockSizeTest >>>(uSemanticTestCases, uSemanticTestRandomTrees,uSemanticTestCasesNew,
                config.populationSize, config.nrowTest, sizeElementsSemanticTest, generation, indexRandomTrees, dStructMutation);
            cudaErrorCheck("geometricSemanticMutation");

            cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSizeTest, computeError, 0, config.populationSize); 
            gridSizeTest = (config.populationSize + blockSizeTest - 1) / blockSizeTest;

            /*!< invokes the GPU to calculate the error (RMSE) the new population*/
            computeError<<< gridSizeTest,blockSizeTest >>>(uSemanticTestCasesNew, uDataTestTarget, uFitTestNew, config.nrowTest);
            cudaErrorCheck("computeError");
            
            /*!< set byte values*/
            cudaMemset(indexRandomTrees,0,twoSizeMemPopulation);
            cudaDeviceSynchronize();
            
            /*!< this section performs survival by updating the semantic and fitness vectors respectively*/
            if(uFitNew[indexBestOffspring] > uFit[indexBestIndividual])
            {
                for (int i = 0; i < config.nrow; ++i)
                {
                    uSemanticTrainCasesNew[indexWorstOffspring*config.nrow+i] = uSemanticTrainCases[indexBestIndividual*config.nrow+i];
                }

                uFitNew[indexWorstOffspring] = uFit[indexBestIndividual];
                
                tempFitnes = uFit;
                uFit = uFitNew;
                uFitNew = tempFitnes;
                tempSemantic = uSemanticTrainCases;
                uSemanticTrainCases = uSemanticTrainCasesNew;
                uSemanticTrainCasesNew = tempSemantic;

                dStructSurvivor[generation].initializePopulationParent = dStructMutation[indexBestIndividual].initializePopulationParent;
                dStructSurvivor[generation].firstParent = dStructMutation[indexBestIndividual].firstParent;
                dStructSurvivor[generation].secondParent = dStructMutation[indexBestIndividual].secondParent;
                dStructSurvivor[generation].newIndividual = dStructMutation[indexBestIndividual].newIndividual;
                dStructSurvivor[generation].mutStep = dStructMutation[indexBestIndividual].mutStep;
                
                for (int j = 0; j < config.nrowTest; ++j)
                {
                    uSemanticTestCasesNew[indexWorstOffspring*config.nrowTest+j] = uSemanticTestCases[indexBestIndividual*config.nrowTest+j];
                }

                uFitTestNew[indexWorstOffspring] = uFitTest[indexBestIndividual];
                
                tempFitnesTest = uFitTest;
                uFitTest = uFitTestNew;
                uFitTestNew = tempFitnesTest;
                tempSemanticTest = uSemanticTestCases;
                uSemanticTestCases = uSemanticTestCasesNew;
                uSemanticTestCasesNew = tempSemanticTest;
                indexBestIndividual = indexWorstOffspring;
            }else
            {
                tempFitnes = uFit;
                uFit = uFitNew;
                uFitNew = tempFitnes;
                tempSemantic = uSemanticTrainCases;
                uSemanticTrainCases = uSemanticTrainCasesNew;
                uSemanticTrainCasesNew = tempSemantic;

                tempFitnesTest = uFitTest;
                uFitTest = uFitTestNew;
                uFitTestNew = tempFitnesTest;
                tempSemanticTest = uSemanticTestCases;
                uSemanticTestCases = uSemanticTestCasesNew;
                uSemanticTestCasesNew = tempSemanticTest;

                dStructSurvivor[generation].initializePopulationParent = dStructMutation[indexBestIndividual].initializePopulationParent;
                dStructSurvivor[generation].firstParent = dStructMutation[indexBestIndividual].firstParent;
                dStructSurvivor[generation].secondParent = dStructMutation[indexBestIndividual].secondParent;
                dStructSurvivor[generation].newIndividual = dStructMutation[indexBestIndividual].newIndividual;
                dStructSurvivor[generation].mutStep = dStructMutation[indexBestIndividual].mutStep;
                indexBestIndividual = indexBestOffspring;
            }
            /*!< writing the  training fitness of the best individual on the file fitnesstrain.csv*/
            fitTraining<<uFit[indexBestIndividual]<<endl;
            /*!< writing the  test fitness of the best individual on the file fitnesstest.csv*/
            fitTesting<<uFitTest[indexBestIndividual]<<endl;
            cudaEventRecord(stopGsgp);
            cudaEventSynchronize(stopGsgp);
            cudaEventElapsedTime(&generationTime, startGsgp, stopGsgp);    
        }

        saveTrace(dStructSurvivor,config.maxNumberGenerations);
        /*!< at the end of the execution  to deallocate memory*/
        cudaFree(indexRandomTrees);
        cudaFree(dStructMutation);
        cudaFree(dStructSurvivor);
        cudaFree(State);
        cublasDestroy(handle);
        cudaFree(dInitialPopulation);
        cudaFree(dRandomTrees);
        free(hInitialPopulation);
        cudaFree(uDataTrainTarget);
        cudaFree(uDataTestTarget);
        cudaFree(uFit);
        cudaFree(uFitNew);
        cudaFree(uSemanticTrainCases);
        cudaFree(uSemanticRandomTrees);
        cudaFree(uSemanticTrainCasesNew);
        cudaFree(uSemanticTestCases);
        cudaFree(uSemanticTestRandomTrees);
        cudaFree(uSemanticTestCasesNew);     
        cudaFree(uFitTest);
        cudaFree(uFitTestNew);
        cudaEventRecord(stopRun);
        cudaEventSynchronize(stopRun);
        cudaEventElapsedTime(&executionTime, startRun, stopRun);
        /*!< writing the time execution for stages the algorithm*/
        times << runs_
        << "," << config.populationSize
        << "," << sizeMaxDepthIndividual 
        << "," << config.nrow 
        << "," << config.nvar 
        << "," << executionTime/1000
        << "," << initialitionTimePopulation/1000
        << "," << timeComputeSemantics/1000
        << "," << generationTime/1000
        <<endl;
    }
    cudaFree(states);
    /*!< all device allocations are removed*/
    cudaDeviceReset();
    return 0;
}


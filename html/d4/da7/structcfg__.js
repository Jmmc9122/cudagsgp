var structcfg__ =
[
    [ "maxDepth", "d4/da7/structcfg__.html#a7f1046b639b1e6307d10949a2bf5ed35", null ],
    [ "maxNumberGenerations", "d4/da7/structcfg__.html#a4025a2ada86fcea97c896e67fc41b44f", null ],
    [ "maxRandomConstant", "d4/da7/structcfg__.html#ae24c1b4a8ab9fe8571ccf931748fa4d3", null ],
    [ "nrow", "d4/da7/structcfg__.html#af95f259e55617065a7c2f200e39ba93f", null ],
    [ "nrowTest", "d4/da7/structcfg__.html#ac87fac8e96a5e2d32fa20dc7783e72f9", null ],
    [ "numberRuns", "d4/da7/structcfg__.html#ab0c8f7373ece340822d32efba04b7e01", null ],
    [ "nvar", "d4/da7/structcfg__.html#aaf8206fc15ba42f538c17a951f8c35da", null ],
    [ "nvarTest", "d4/da7/structcfg__.html#a04803290cbeff55d18e679b20b471030", null ],
    [ "populationSize", "d4/da7/structcfg__.html#ae8d24427036d34b3a7d21d7a59ff05ed", null ]
];
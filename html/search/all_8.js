var searchData=
[
  ['readconfigfile_35',['readConfigFile',['../d3/d9b/_gsgp_cuda_8cpp.html#a3ad4f51591d12acf0b3d3fc0badfca4d',1,'readConfigFile(cfg *config):&#160;GsgpCuda.cpp'],['../de/dc7/_gsgp_cuda_8h.html#a81e2e6e609828492493948e200939a42',1,'readConfigFile(cfg *config):&#160;GsgpCuda.cpp']]],
  ['readinpudata_36',['readInpuData',['../d3/d9b/_gsgp_cuda_8cpp.html#a14bc95f02546f2ed56ce69770053714b',1,'readInpuData(char *train_file, char *test_file, float *dataTrain, float *dataTest, float *dataTrainTarget, float *dataTestTarget, int nrow, int nvar, int nrowTest, int nvarTest):&#160;GsgpCuda.cpp'],['../de/dc7/_gsgp_cuda_8h.html#acfa80b09d39c91f2febe825a93e95f1c',1,'readInpuData(char *trainFile, char *testFile, float *dataTrain, float *dataTest, float *dataTrainTarget, float *dataTestTarget, int nrow, int nvar, int nrowTest, int nvarTest):&#160;GsgpCuda.cpp']]],
  ['readme_2emd_37',['README.md',['../d9/dd6/_r_e_a_d_m_e_8md.html',1,'']]]
];

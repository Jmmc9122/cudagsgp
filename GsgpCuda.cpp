/*<one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2020  José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//! \file     GsgpCuda.cpp
//! \brief    file containing the definition of the modules (kernels) used to create the population of individuals, evaluate them, the search operator and read data
//! \date     created on 25/01/2020

#include "GsgpCuda.h"
//#include <__clang_cuda_math_forward_declares.h>

/*!
* \fn       string currentDateTime()
* \brief    function to capture the date and time of the host, this allows to define the exact moment of each GSGP-CUDA run,
            this allows us to name the output files by date and time.
* \return   char: return date and time from the host computer
* \date     25/01/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp
*/
const std::string currentDateTime()
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
  return buf;
}

/*!
* \fn       void cudaErrorCheck(const char* functionName)
* \brief    This function catches the error detected by the compiler and prints the user-friendly error message.
* \param    char funtionName: pointer to the name of the kernel executed to verify if there was an error 
* \return   void
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp
*/
void cudaErrorCheck(const char* functionName)
{
    cudaError_t error = cudaGetLastError();
    if(error != cudaSuccess)
    {
        printf("CUDA error (%s): %s\n", functionName, cudaGetErrorString(error));
        exit(-1);
    }
}

/*!
* \fn       __global__ void init(unsigned int seed, curandState_t* states)
* \brief    This kernel is used to initialize the random states to generate random numbers with a different pseudo sequence in each thread
* \param    int seed: used to generate a random number for each core  
* \param    curandState_t states: pointer to store a random state for each thread
* \return   void
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp
*/
__global__ void init(unsigned int seed, curandState_t* states)
{
  const unsigned int tid = threadIdx.x+blockIdx.x*blockDim.x;
  curand_init(seed, tid, 0, &states[tid]);
}

/*!
* \fn       __device__ int push(float val,int *pushGenes, float *stackInd)
* \brief    push() function is used to insert an element at the top of the stack. The element is added to the stack container and the size of the stack is increased by 1.
* \param    float val: variable that stores a value resulting from a valid operation in the interpreter
* \param    int *pushGenes: auxiliary pointer that stores the positions of individuals 
* \param    float *stackInd: auxiliary pointer that stores the values ​​resulting from the interpretation of each individual
* \return   int: auxiliary pointer that stores the positions of individuals + 1
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp 
*/
__device__ int push(float val, int *pushGenes, float *stackInd)
{
  const unsigned int tid = threadIdx.x+blockIdx.x*blockDim.x;
  stackInd[pushGenes[tid]] = val;
  return pushGenes[tid]+1;
}

/*!
* \fn       __device__ float pop(int *pushGenes, float *stackInd)
* \brief    pop() function is used to remove an element from the top of the stack(newest element in the stack). The element is removed to the stack container and the size of the stack is decreased by 1.
* \param    int *pushGenes: auxiliary pointer that stores the positions of individuals 
* \param    float *stackInd: auxiliary pointer that stores the values ​​resulting from the interpretation of each individual
* \return   float: returns the stackInd without the value positioned in pushGenes [tid]
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp
*/
__device__ float pop(int *pushGenes, float *stackInd)
{
  const unsigned int tid = threadIdx.x+blockIdx.x*blockDim.x;
  pushGenes[tid]--;
  return stackInd[pushGenes[tid]];
}

/*!
* \fn       __device__ bool isEmpty(int *pushGenes, unsigned int sizeMaxDepthIndividual)     
* \brief    Check if a stack is empty
* \param    int *pushGenes: auxiliary pointer that stores the positions of individuals 
* \param    int sizeMaxDepthIndividual: variable thar stores maximum depth for individuals
* \return   bool - true if the stack is empty, false otherwise
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     gsgpMalloc.cpp
*/
__device__ bool isEmpty(int *pushGenes, unsigned int sizeMaxDepthIndividual)
{
  const unsigned int tid = threadIdx.x+blockIdx.x*blockDim.x;
  if (pushGenes[tid] <= tid * sizeMaxDepthIndividual)
    return true;
  else
    return false;
}

/*!
* \fn       __device__ void clearStack(int *pushGenes, unsigned int sizeMaxDepthIndividual, float *stackInd)
* \brief    remove all elements from the stack so that in the next evaluations there are no previous values of other individuals
* \param    int *pushGenes: auxiliary pointer that stores the positions of individuals 
* \param    int sizeMaxDepthIndividual: variable thar stores maximum depth for individuals
* \param    float *stackInd: auxiliary pointer that stores the values ​​resulting from the evaluation of each individual
* \return   void
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp 
*/
__device__ void clearStack(int *pushGenes, unsigned int sizeMaxDepthIndividual, float *stackInd)
{
  const unsigned int tid = threadIdx.x+blockIdx.x*blockDim.x;
  pushGenes[tid] = tid*sizeMaxDepthIndividual;
  for(int i = 0; i< sizeMaxDepthIndividual; i++)
    stackInd[tid*sizeMaxDepthIndividual+i] = 0;
}

/*!
* \fn       __global__ void initializePopulation(float* dInitialPopulation, int nvar, int sizeMaxDepthIndividual, curandState_t* states, int maxRandomConstant)
* \brief    The initializePopulation kernel creates the population of programs T and the set of random trees R uses by the GSM kernel, based on the desired population
            size and maximun program length. The individuals are representd using a linear genome, composed of valid terminals (inputs to the program) and functions 
            (basic elements with which programs can be built).
* \param    float *dInitialPopulation: vector pointers to store the individuals of the initial population
* \param    int nvar: variable containing the number of columns (excluding the target) of the training dataset
* \param    int sizeMaxDepthIndividual: variable thar stores maximum depth for individuals
* \param    curandState_t *states: random status pointer to generate random numbers for each thread
* \param    int maxRandomConstant: variable containing the maximum number to generate ephemeral constants
* \return   void
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp
*/
__global__ void initializePopulation(float* dInitialPopulation, int nvar, int sizeMaxDepthIndividual, curandState_t* states, int maxRandomConstant, int functions)
{
  const unsigned int tid = threadIdx.x+blockIdx.x*blockDim.x;
      for (unsigned int j = 0; j < sizeMaxDepthIndividual; j++)
      {
          if(curand_uniform(&states[tid])<0.5)
          {
             dInitialPopulation[tid*sizeMaxDepthIndividual+j] = (curand(&states[tid]) % functions + 1)*(float)(-1);
          }  
          else
          {
            if (curand_uniform(&states[tid])<0.3)
            {
              dInitialPopulation[tid*sizeMaxDepthIndividual+j] = (curand(&states[tid]) % maxRandomConstant+1);
            }
            else
            {
              dInitialPopulation[tid*sizeMaxDepthIndividual+j] = (curand(&states[tid]) % nvar+1000)*(float)(-1);
            }
          }
      }
}

/*!
* \fn       __global__ void computeSemantics    
* \brief    The ComputeSemantics kernel is an interpreter, that decodes each individual and evaluates it over all fitness cases,
            producing as output the semantic vector of each individual. The chromosome is interpreted linearly, using an auxiliary LIFO stack D that stores 
            terminals from the chromosome and the output from valid operations.
* \param    float *inputPopulation: vector pointers to store the individuals of the population
* \param    float *outSemantic: vector pointers to store the semantics of each individual in the population
* \param    int sizeMaxDepthIndividual: variable thar stores maximum depth for individuals
* \param    float *data: pointer vector containing training or test data
* \param    int nrow: variable containing the number of rows (instances) of the training dataset
* \param    int nvar: variable containing the number of columns (excluding the target) of the training dataset
* \param    int *pushGenes: auxiliary pointer that stores the positions of individuals 
* \param    float *stackInd: auxiliary pointer that stores the values ​​resulting from the interpretation of each individual
* \return   void
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp 
*/
__global__ void computeSemantics(float *inputPopulation, float *outSemantic, unsigned int sizeMaxDepthIndividual, float *data,
 int nrow, int nvar, int *pushGenes, float *stackInd)
{

  const unsigned int tid = threadIdx.x+blockIdx.x*blockDim.x;
  pushGenes[tid] = tid * sizeMaxDepthIndividual;
  int t,t_;
  float tmp,tmp2,out;
  for(int k=0; k<nrow; k++)
  {
    out=0;
    clearStack(pushGenes,sizeMaxDepthIndividual, stackInd);
    for(int i=0; i < sizeMaxDepthIndividual; i++)
    {

      if(inputPopulation[tid*sizeMaxDepthIndividual+i] > 0)
      {
        pushGenes[tid] = push(inputPopulation[tid*sizeMaxDepthIndividual+i],pushGenes,stackInd);
      }
      else 
        if(inputPopulation[tid*sizeMaxDepthIndividual+i] <= -1000)
        {
          t=inputPopulation[tid*sizeMaxDepthIndividual+i];
          t_=(t+1000)*(-1);
          pushGenes[tid] = push(data[t_+nvar*k],pushGenes,stackInd);
        }   
        else 
          if (inputPopulation[tid*sizeMaxDepthIndividual+i] == -1)
          {
            if (!isEmpty(pushGenes,sizeMaxDepthIndividual))
            {
              tmp = pop(pushGenes,stackInd);
              if (!isEmpty(pushGenes,sizeMaxDepthIndividual))
              {
                tmp2 = pop(pushGenes,stackInd);
                if (!isnan(tmp) || !isnan(tmp2))
                {
                  pushGenes[tid] = push(tmp2 + tmp,pushGenes,stackInd);
                  out = tmp2+tmp;
                }
              }
              else
                pushGenes[tid] = push(tmp,pushGenes,stackInd);
            }
          }
          else 
            if (inputPopulation[tid*sizeMaxDepthIndividual+i] == -2)
            {
              if(!isEmpty(pushGenes,sizeMaxDepthIndividual))
              {
                tmp = pop(pushGenes,stackInd);
                  if (!isEmpty(pushGenes,sizeMaxDepthIndividual))
                  {
                      tmp2 = pop(pushGenes,stackInd);
                  if (!isnan(tmp) || !isnan(tmp2))
                  {
                    pushGenes[tid] = push(tmp2 - tmp,pushGenes,stackInd);
                    out = tmp2-tmp;
                  }
              }
            }else
              pushGenes[tid] = push(tmp,pushGenes,stackInd);
            }
            else
              if (inputPopulation[tid*sizeMaxDepthIndividual+i] == -3)
              {
                if (!isEmpty(pushGenes,sizeMaxDepthIndividual))
                {
                  tmp = pop(pushGenes,stackInd);
                  if (!isEmpty(pushGenes,sizeMaxDepthIndividual))
                  {
                    tmp2 = pop(pushGenes,stackInd);
                    if (!isnan(tmp) || !isnan(tmp2))
                    {
                      pushGenes[tid] = push(tmp2 * tmp,pushGenes,stackInd);
                      out = tmp2*tmp;
                  }
              }
          }else
          pushGenes[tid] = push(tmp,pushGenes,stackInd);
       }
       else
        if (inputPopulation[tid*sizeMaxDepthIndividual+i] == -4)
        {
          if (!isEmpty(pushGenes,sizeMaxDepthIndividual))
          {
              tmp = pop(pushGenes,stackInd);
              if (!isEmpty(pushGenes,sizeMaxDepthIndividual))
              {
                tmp2 = pop(pushGenes,stackInd);
                if (!isnan(tmp) || !isnan(tmp2))
                {
                  pushGenes[tid] = push(tmp2 / sqrtf(1+tmp*tmp),pushGenes,stackInd);
                  out = tmp2 / sqrtf(1+tmp*tmp);
                }
              }
          }else
            pushGenes[tid] = push(tmp,pushGenes,stackInd);
      }
    }
      outSemantic[(tid*nrow+k)] = out;
  } 
}

/*!
* \fn       __global__ void computeError(float *semantics, float *targetValues, float *fit, int nrow)
* \brief    The computeError kernel computes the RMSE between each row of the semantic matrix ST,m×n and the target vector t, computing the
            fitness of each individual in the population.
* \param    float *semantics: vector of pointers that contains the semantics of the individuals of the initial population 
* \param    float *targetValues: pointer containing the target values of train or test
* \param    float *fit: vector that will store the error of each individual in the population
* \param    int nrow: variable containing the number of rows (instances) of the training and test dataset
* \return   void
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     gsgpMalloc.cpp
*/
__global__ void computeError(float *semantics, float *targetValues, float *fit, int nrow)
{
  const unsigned int tid = threadIdx.x+blockIdx.x*blockDim.x;
  float temp = 0;
    for(int i=0; i<nrow; i++)
    {
      temp += (semantics[tid*nrow+i]-targetValues[i])*(semantics[tid*nrow+i]-targetValues[i]);  
    }
  temp = sqrtf(temp/nrow);
  fit[tid] = temp;
}

/*!
* \fn       __device__ float sigmoid(float n)
* \brief    auxiliary function for the geometric semantic mutation operation
* \param    float n: semantic value of a random tree
* \return   float n: value resulting from the function
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp  
*/
__device__ float sigmoid(float n)
{
  return 1.0/(1+expf(-1*(n)));
}

/*!
* \fn       __global__ void initializeIndexRandomTrees(int sizePopulation, float *indexRandomTrees, curandState_t* states)
* \brief    this kernel generates random indexes for random trees that are used in the mutation operator to select two random trees.
* \param    int sizePopulation: this variable contains the number of individuals that the population has
* \param    float *indexRandomTrees: this pointer stores the indexes randomly for mutation
* \param    curandState_t* states: random status pointer to generate random numbers for each thread
* \return   void
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp  
*/              
__global__ void initializeIndexRandomTrees(int sizePopulation, float *indexRandomTrees, curandState_t* states)
{
  const unsigned int tid = threadIdx.x+blockIdx.x*blockDim.x;
  indexRandomTrees[tid] = (curand(&states[tid]) % sizePopulation);
}

/*!
* \fn       __global__ void geometricSemanticMutation(float *initialPopulationSemantics, float *randomTreesSemantics, float *newSemanticsOffsprings, int sizePopulation, int nrow, int tElements, int generation, float *indexRandomTrees, entry_ *x)
* \brief    The GSM operator is basically a vector addition operation, that can be performed independently for each semantic element STi,j.
            However, it is necessary to select the semantics of two random trees R u and R v , and a random mutation step ms.
* \param    float *initialPopulationSemantics: this vector of pointers contains the semantics of the initial population
* \param    float *randomTreesSemantics: this vector of pointers contains the semantics of the random trees
* \param    float *newSemanticsOffsprings: this vector of pointers will store the semantics of the new offspring
* \param    int sizePopulation: this variable contains the number of individuals that the population has
* \param    int nrow: variable containing the number of rows (instances) of the training dataset
* \param    int tElements: variables containing the total number of semantic elements
* \param    int generation: number of generation
* \param    float *indexRandomTrees: this pointer stores the indexes randomly for mutation
* \param    struc *x: variable used to store training and test instances 
* \return   void
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp
*/
__global__ void geometricSemanticMutation(float *initialPopulationSemantics, float *randomTreesSemantics, float *newSemanticsOffsprings, int sizePopulation,
  int nrow, int tElements, int generation, float *indexRandomTrees, entry_ *x)
{
  const unsigned int tid = threadIdx.x+blockIdx.x*blockDim.x;
  int nSeed = (tid/nrow);
  int firstTree = indexRandomTrees[nSeed], secondTree = indexRandomTrees[sizePopulation + nSeed];
  curandState_t state;
  //curand_init(firstTree*generation, 0, 0, &state);
  curand_init(generation, 0, 0, &state);
  float ms= curand_uniform(&state);
  x[tid%sizePopulation].initializePopulationParent=tid%sizePopulation;
  x[tid%sizePopulation].firstParent=firstTree;
  x[tid%sizePopulation].secondParent=secondTree;
  x[tid%sizePopulation].newIndividual=tid;
  x[tid%sizePopulation].mutStep=ms;
  newSemanticsOffsprings[tid] = initialPopulationSemantics[tid]+(ms)*((1.0/(1+expf(-(randomTreesSemantics[firstTree*nrow+tid%nrow]))))-(1.0/(1+expf(-(randomTreesSemantics[secondTree*nrow+tid%nrow])))));
}

/*!
* \fn       __host__ void saveTrace(entry *structSurvivor, int generation) 
* \brief    Function that stores the information related to the evolutionary cycle and stores the indices of the individuals that were used in each generation to create new offspring,
            to later perform the reconstruction of the optimal solution in the trace.txt file
* \param    struc *structSurvivor: pointer that stores information of the best individual throughout the generations
* \param    int generation: number of generations
* \return   void
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp  
*/
__host__ void saveTrace(entry *structSurvivor, int generation){
  ofstream trace("trace.txt",ios::out);
  for(int unsigned i=1; i<=generation; i++)
  {
      trace << structSurvivor[i].initializePopulationParent<<"\t" << structSurvivor[i].firstParent << "\t" << structSurvivor[i].secondParent << "\t" << structSurvivor[i].newIndividual <<"\t"<< structSurvivor[i].mutStep<<endl;
      if(i<generation-1)
         trace<<"***"<<endl;
      else  
      trace<<"***"<<endl;
  } 
}

/*!
* \fn        __host__ void readInpuData(char *train_file, char *test_file, float *dataTrain, float *dataTest, float *dataTrainTarget,float *dataTestTarget, int nrow, int nvar, int nrowTest, int nvarTest)
* \brief    function that reads data from training and test files, also reads target values to store them in pointer vectors.
* \param    char *train_file: name of the file with training instances 
* \param    char *test_file: name of the file with test instances
* \param    float *dataTrain: vector pointers to store training data
* \param    float *dataTest: vector pointers to store test data
* \param    float *dataTrainTarget: vector pointers to store training target data
* \param    float *dataTestTarget: vector pointers to store test target data
* \param    int nrow: variable containing the number of rows (instances) of the training dataset
* \param    int nvar: variable containing the number of columns (excluding the target) of the training dataset
* \param    int nrowTest: variable containing the number of rows (instances) of the test dataset
* \param    int nvarTest: variable containing the number of columns (excluding the target) of the test dataset
* \return   void: 
* \date     01/25/2020
* \author   José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file     GsgpCuda.cpp 
*/
__host__ void readInpuData(char *train_file, char *test_file, float *dataTrain, float *dataTest, float *dataTrainTarget,
 float *dataTestTarget, int nrow, int nvar, int nrowTest, int nvarTest){
  std::fstream in(train_file,ios::in);
  if (!in.is_open())
  {
    cout<<endl<<"ERROR: TRAINING FILE NOT FOUND." << endl;
    exit(-1);
  }
  std::fstream inTest(test_file,ios::in);
  if (!in.is_open())
  {
    cout<<endl<<"ERROR: TEST FILE NOT FOUND." << endl;
    exit(-1);
  }
  char Str[1024];
  int max = nvar;
  for(int i=0;i<nrow;i++)
  {
    for (int j=0; j<nvar+1; j++)
    {
      if (j==max)
      {
        in>>Str;
        dataTrainTarget[i]=atof(Str);
      }
      if (j<nvar)
      {
        in>>Str;
        dataTrain[i*nvar+j] = atof(Str);
      }
    }
  }
  in.close();
  int maxTest = nvarTest;
  for(int i=0;i<nrowTest;i++)
  {
    for (int j=0; j<nvarTest+1; j++)
    {
      if (j==maxTest)
      {
        inTest>>Str;
        dataTestTarget[i]=atof(Str);
      }
      if (j<nvarTest)
      {
        inTest>>Str;
        dataTest[i*nvarTest+j] = atof(Str);
      }
    }
  }
  inTest.close();
}

/*!
* \fn        __host__ void readConfigFile(cfg *config)
* \brief     Function that reads the configuration file where the parameters are found to initialize the algorithm.
* \param     cfg *config: pointer to the struct containing the variables needed to run the program
* \return    void
* \date      01/25/2020
* \author    José Manuel Muñoz Contreras, Leonardo Trujillo, Daniel E. Hernandez, Perla Juárez Smith
* \file      GsgpCuda.cpp
*/
__host__ void readConfigFile(cfg *config){
  std::fstream f("configuration.ini", ios::in);
  if (!f.is_open())
  {
    cerr<<"CONFIGURATION FILE NOT FOUND." << endl;
    exit(-1);
  }
  int k=0;
  while(!f.eof())
  {
    char str[100]="";
    char str2[100]="";
    int j=0;
    f.getline(str,100);
    if(str[0]!='\0')
    {
      while(str[j]!='=')
      {
        j++;
      }
      j++;
      int i=0;
      while(str[j]==' ')
      {
        j++;
      }
      while(str[j]!='\0')
      {
        str2[i] = str[j];
        j++;
        i++;
      }
    }
    if(k==0)
      config->numberRuns = atoi(str2);
    if(k==1)
      config->maxNumberGenerations=atoi(str2); 
    if(k==2)
      config->populationSize =atoi(str2);
    if(k==3)
      config->maxDepth=atof(str2);
    if(k==4)
      config->nrow=atof(str2);
    if(k==5)  
      config->nvar=atoi(str2);
    if(k==6)
      config->nrowTest=atof(str2);
    if(k==7)  
      config->nvarTest=atoi(str2);
    if(k==8)  
      config->maxRandomConstant=atoi(str2);
      k++;        
  } 
    f.close();
    if(config->populationSize<0 || config->maxDepth<0 )
    {
        cout<<"ERROR: POPULATION SIZE AND MAX DEPTH MUST BE SMALLER THAN (OR EQUAL TO) 0 AND THEIR SUM SMALLER THAN (OR EQUAL TO) 1.";
        exit(-1);
    }
}

__host__ void saveIndividuals(float *hInitialPopulation, int sizeMaxDepthIndividual, int populationSize){
  ofstream trace("individuals",ios::out);
    for (int i=0; i< populationSize; i++) {
        trace << i << ": ";
        for (int j=0; j<sizeMaxDepthIndividual; j++) {
            trace<< hInitialPopulation[i*sizeMaxDepthIndividual+j] << " ";         
         }
         trace<< endl;     
     }
}

